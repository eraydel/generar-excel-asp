<%@ LANGUAGE=VBSCRIPT CODEPAGE="65001"%>
<!-- #include file="Excel_Reporter.asp" -->
<!-- #include file="header_excel.asp" -->


<!-- Creando el objeto para impresión en excel de los anexos-->
<% set objExcel = new Excel_Reporter %>

<!-- Título de la hoja -->
objExcel.sheetName = "DATOS GENERALES"

<!-- Define encabezado -->
headTables = array("Columna A" , "Columna B")

<%
objExcel.setColumnWidth = "70" 
objExcel.setMergeColumn = "6"

	objExcel.openWorkSheet() 
		objExcel.openTable()
			
			objExcel.sectionCategory("INFORMACIÓN DEL SOLICITANTE")
			objExcel.setNewTable()
			'Manda a llamar el encabezado
			objExcel.setHeadTable( headTables )
			'hay que enviar una matriz de datos para pintar la información de la forma (matriz[renglon ,columna])
			objExcel.setBodyTable( matriz )	
		objExcel.closeTable()
		objExcel.WorksheetOptions() 
    objExcel.closeWorkSheet() 

        
%>
</Workbook>

<%

' ***************************************
' Nombre de Clase 	: Excel_Reporter
' Type 				: Controller
' Created by 		: Erick Ayala Delgadillo
' Created date 		: 11-Feb-2013
' Description 		: Clase que permite la impresión de la cédula de información fiscal
' Versión: 0.1
' Updated by 		:
' Updated at 		:
' ***************************************

Class Excel_Reporter
	
	
	'**********************************************************************************************
	' Definición de propiedades
	private sheet_name 		' Determina el nombre de la hoja
	private sheet_title 	' Determina el titulo de la hoja
	private sheet_subtitle 	' Determina el subtitulo de la hoja
	private actividad
	
	' Sheet Properties
	private column_width  ' ancho de columna
	private row_height
	private merge_column  ' combinar celdas
	private total		  ' Columna de totales
	private total_iva
	
	private position
	private columnNum
	private repeater
	
	'Constructor de la clase
	public sub Class_Initialize()
		column_width 	= 60
		row_height 		= 24.75
		merge_column 	= 11
		merge_row 		= 7
		total 			= 0
		position 		= 0
		columnNum 		= 0
		rowNum 			= 0
		repeater 		= 0
		total_iva 		= 0
		actividad 		= "No Especificada"
	end sub
	
	public property let sheetName( titulo )
		sheet_name = titulo
	end property
	
	public property let setTitle( title )
		sheet_title = title
	end property
	
	public property let setSubtitle( subtitle )
		sheet_subtitle = subtitle
	end property
	
	public property let setColumnWidth( width )
		column_width = width
	end property
	
	public property let setRowHeight( height )
		row_height = height
	end property
	
	public property let setMergeColumn( width )
		merge_column = width
	end property
	
	public property let setTotal( suma )
		total = suma
	end property
	
	public property let setTotalIva( tot )
		total_iva = tot
	end property
	
	public property let setColumnPosition( pos )
		position = pos
	end property
	
	public property let setRowPosition( posr )
		positionR = posr
	end property
	
	public property let setColumnNum( num )
		columnNum = num
	end property
	
	public property let setRowNum( num )
		RowNum = num
	end property
	
	public property let setRepeaterUntil( repeat )
		repeater = repeat
	end property
	
	public property let setActivity( actv_ )
		actividad = actv_
	end property
	
	
	property get getSheetName()
		getSheetName = sheet_name
	end property
	
	property get getTitle()
		getTitle = sheet_title
	end property
	
	property get getSubtitle()
		getSubtitle = sheet_subtitle
	end property
	
	property get getColumnWidth()
		getColumnWidth = column_width
	end property
	
	property get getMergeColumn()
		getMergeColumn = merge_column
	end property
		property get getMergeRow()
		getMergeRow = merge_row
	end property
	
	property get getTotal()
		getTotal = total
	end property
	
	property get getTotalIva()
		getTotalIva = total_iva
	end property
	
	property get getRepeaterUntil()
		getRepeaterUntil = repeater
	end property
	
	property get getActivity()
		getActivity = actividad
	end property
	
	
	public function setHeadTable( arr )
	
	 	if( isArray( arr ) ) then
			
					Response.Write("<Row ss:Height='"&row_height&"'>")
					for indice = 0 to Ubound( arr )	
						if( indice = 0 ) then
								Response.Write("<Cell ss:MergeAcross='0' ss:StyleID='s87'>")
								Response.Write("</Cell>")
						else
							if( indice = 1 ) then
								Response.Write("<Cell ss:MergeAcross='7' ss:StyleID='s87'>")
									Response.Write("<Data ss:Type='String'>Personas Físicas</Data>")
								Response.Write("</Cell>")
							end if
							if( indice = 2 ) then
								Response.Write("<Cell ss:MergeAcross='7' ss:StyleID='s87'>")
									Response.Write("<Data ss:Type='String'>Personas Morales</Data>")
								Response.Write("</Cell>")
							end if
							if( indice = 3) then
								Response.Write("<Cell ss:MergeAcross='7' ss:StyleID='s87'>")
									Response.Write("<Data ss:Type='String'>Dictaminados</Data>")
								Response.Write("</Cell>")
							end if
						end if
					next 
					Response.Write("</Row>")
					
			Response.Write("<Row ss:Height='"&row_height&"'>")
				for indice = 0 to Ubound( arr )	
					if( position = indice and postion <= getRepeaterUntil() ) then
						Response.Write("<Cell ss:MergeAcross='"&columnNum&"' ss:StyleID='s87'>")
						if( getRepeaterUntil() > 0 ) then
							position = indice +1 
						end if
					else 
					Response.Write("<Cell ss:StyleID='s87'>")
					end if
						Response.Write("<Data ss:Type='String'>"&arr(indice)&"</Data>")
					Response.Write("</Cell>")
				next
				position = 1
			Response.Write("</Row>")
		end if
	
	end function
	

		
	public function setBodyTable( matriz )
		if( isArray( matriz ) ) then
			for renglones = 0 to uBound( matriz , 2 )
				Response.Write("<Row ss:Height='24.75'>")
					Response.Write("<Cell ss:StyleID='s88'>")				
						Response.Write("<Data ss:Type='String'>"&matriz( 0, renglones )&"</Data>")
					Response.Write("</Cell>")
					Response.Write("<Cell ss:StyleID='sNumerico'>")
						if( matriz( 13, renglones ) > 0 ) then 				
							Response.Write("<Data ss:Type='Number'>"&formatNumber(matriz( 13, renglones ),2)&"</Data>")
						end if
					Response.Write("</Cell>")
					Response.Write("<Cell ss:StyleID='s88'>")				
					Response.Write("<Data ss:Type='String'>"& UCASE(matriz( 14, renglones ))&"</Data>")
					Response.Write("</Cell>")
					
					Response.Write("<Cell ss:StyleID='sNumerico'>")
						if( matriz( 15, renglones ) > 0 ) then 				
							Response.Write("<Data ss:Type='Number'>"&formatNumber(matriz( 15, renglones ),2)&"</Data>")
						end if
					Response.Write("</Cell>")
					Response.Write("<Cell ss:StyleID='s88'>")				
					Response.Write("<Data ss:Type='String'>"&UCASE(matriz( 16, renglones ))&"</Data>")
					Response.Write("</Cell>")
					
					Response.Write("<Cell ss:StyleID='sNumerico'>")
						if( matriz( 17, renglones ) > 0 ) then 				
							Response.Write("<Data ss:Type='Number'>"&formatNumber(matriz( 17, renglones ),2)&"</Data>")
						end if
					Response.Write("</Cell>")
					Response.Write("<Cell ss:StyleID='s88'>")				
					Response.Write("<Data ss:Type='String'>"&UCASE(matriz( 18, renglones ))&"</Data>")
					Response.Write("</Cell>")
					
					Response.Write("<Cell ss:StyleID='sNumerico'>")
						if(matriz( 19, renglones ) > 0 ) then 				
							Response.Write("<Data ss:Type='Number'>"&formatNumber(matriz( 19, renglones ),2)&"</Data>")
						end if
					Response.Write("</Cell>")
					Response.Write("<Cell ss:StyleID='s88'>")				
					Response.Write("<Data ss:Type='String'>"&UCASE(matriz( 20, renglones ))&"</Data>")
					Response.Write("</Cell>")
					
					Response.Write("<Cell ss:StyleID='sNumerico'>")
						if(matriz( 21, renglones ) > 0 ) then 				
							Response.Write("<Data ss:Type='Number'>"&formatNumber(matriz( 21, renglones ),2)&"</Data>")
						end if
					Response.Write("</Cell>")
					Response.Write("<Cell ss:StyleID='s88'>")				
					Response.Write("<Data ss:Type='String'>"&UCASE(matriz( 22, renglones ))&"</Data>")
					Response.Write("</Cell>")
					
					Response.Write("<Cell ss:StyleID='sNumerico'>")
						if(matriz( 23, renglones ) > 0 ) then 				
							Response.Write("<Data ss:Type='Number'>"&formatNumber(matriz( 23, renglones ),2)&"</Data>")
						end if
					Response.Write("</Cell>")
					Response.Write("<Cell ss:StyleID='s88'>")				
					Response.Write("<Data ss:Type='String'>"&UCASE(matriz( 24, renglones ))&"</Data>")
					Response.Write("</Cell>")
					
					Response.Write("<Cell ss:StyleID='sNumerico'>")
						if(matriz( 25, renglones ) > 0 ) then 				
							Response.Write("<Data ss:Type='Number'>"&formatNumber(matriz( 25, renglones ),2)&"</Data>")
						end if
					Response.Write("</Cell>")
					Response.Write("<Cell ss:StyleID='s88'>")				
					Response.Write("<Data ss:Type='String'>"&UCASE(matriz( 26, renglones ))&"</Data>")
					Response.Write("</Cell>")
					
					
					Response.Write("<Cell ss:StyleID='sNumerico'>")
						if(matriz( 27, renglones ) > 0 ) then 				
							Response.Write("<Data ss:Type='Number'>"&formatNumber(matriz( 27, renglones ),2)&"</Data>")
						end if
					Response.Write("</Cell>")
					Response.Write("<Cell ss:StyleID='s88'>")				
					Response.Write("<Data ss:Type='String'>"&UCASE(matriz( 28, renglones ))&"</Data>")
					Response.Write("</Cell>")

					Response.Write("<Cell ss:StyleID='sNumerico'>")
						if(matriz( 29, renglones ) > 0 ) then 				
							Response.Write("<Data ss:Type='Number'>"&formatNumber(matriz( 29, renglones ),2)&"</Data>")
						end if
					Response.Write("</Cell>")
					Response.Write("<Cell ss:StyleID='s88'>")				
					Response.Write("<Data ss:Type='String'>"&UCASE(matriz( 30, renglones ))&"</Data>")
					Response.Write("</Cell>")
					
					Response.Write("<Cell ss:StyleID='sNumerico'>")
						if(matriz( 31, renglones ) > 0 ) then 				
							Response.Write("<Data ss:Type='Number'>"&formatNumber(matriz( 31, renglones ),2)&"</Data>")
						end if
					Response.Write("</Cell>")
					Response.Write("<Cell ss:StyleID='s88'>")				
					Response.Write("<Data ss:Type='String'>"&UCASE(matriz( 32, renglones ))&"</Data>")
					Response.Write("</Cell>")	
					
					Response.Write("<Cell ss:StyleID='sNumerico'>")
						if(matriz( 33, renglones ) > 0 ) then 				
							Response.Write("<Data ss:Type='Number'>"&formatNumber(matriz( 33, renglones ),2)&"</Data>")
						end if
					Response.Write("</Cell>")
					Response.Write("<Cell ss:StyleID='s88'>")				
					Response.Write("<Data ss:Type='String'>"&UCASE(matriz( 34, renglones ))&"</Data>")
					Response.Write("</Cell>")	
					
					Response.Write("<Cell ss:StyleID='sNumerico'>")
						if(matriz( 35, renglones ) > 0 ) then 				
							Response.Write("<Data ss:Type='Number'>"&formatNumber(matriz( 35, renglones ),2)&"</Data>")
						end if
					Response.Write("</Cell>")
					
					Response.Write("<Cell ss:StyleID='s88'>")				
					Response.Write("<Data ss:Type='String'>"&UCASE(matriz( 36, renglones ))&"</Data>")
					Response.Write("</Cell>")		
				Response.Write("</Row>")
			next
		else
			Response.Write("<Row ss:Height='24.75'>")
			Response.Write("<Cell ss:MergeAcross='"&getMergeColumn() - 1&"' ss:StyleID='s88'>")
			Response.Write("<Data ss:Type='String'>- No hay información para mostrar -</Data>")
			Response.Write("</Cell>")
			Response.Write("</Row>")
		end if
		sheet = sheet & "<Row>"
    	sheet = sheet & "<Cell ss:MergeAcross='"&getMergeColumn() - 1&"' ss:StyleID='s86'>"
		sheet = sheet & "<Data ss:Type='String'></Data>"
		sheet = sheet & "</Cell>"
    	sheet = sheet & "</Row>"
		Response.Write( sheet )
	end function


public function openTable()
	 sheet = sheet & "<Table ss:ExpandedColumnCount='50' ss:ExpandedRowCount='65000' x:FullColumns='1' "
     sheet = sheet & "x:FullRows='1' ss:DefaultColumnWidth='"&getColumnWidth()&"' ss:DefaultRowHeight='15'>"
	 Response.Write( sheet )
end function
	
public function setNewTable()
		sheet = sheet & "<Row>"
    	sheet = sheet & "<Cell ss:MergeAcross='"&getMergeColumn() - 1&"' ss:StyleID='s86'>"
		sheet = sheet & "<Data ss:Type='String'>"&getSubtitle()&"</Data>"
		sheet = sheet & "</Cell>"
    	sheet = sheet & "</Row>"
		Response.Write( sheet )
end function

public function closeTable()
		Response.Write("</Table>")
end function
	
public function openWorkSheet()
	 	sheet = "<Worksheet ss:Name='"&getSheetName()&"'>"
		Response.Write( sheet )
end function
	
public function closeWorkSheet()
		Response.Write("</Worksheet>")
end function
	
' Define propiedades para las hojas de excel'
public function WorksheetOptions()
		options = "<WorksheetOptions xmlns='urn:schemas-microsoft-com:office:excel'>"
	   	options = options & "<PageSetup>"
    	options = options & "<Layout x:Orientation='Landscape'/>"
	    options = options & "<Header x:Margin='0.3' "
     	options = options & "x:Data='&amp;C&amp;&quot;Calibri,Negrita&quot;ADMINISTRACIÓN GENERAL DE PLANEACIÓN&amp;&quot;Calibri,Normal&quot;&#10;MODELO DE EVASIÓN DEL CONTRIBUYENTE - MEC / SIMETRÍA FISCAL DE PROVEEDORES &#10;&amp;&quot;Calibri,Negrita&quot;&amp;A&amp;&quot;Calibri,Normal&quot;&#10;'/>"
		options = options & "<Footer x:Margin='0.3' x:Data='&amp;Z&amp;6FECHA DE GENERACIÓN:&amp;&quot;Calibri,Negrita&quot;&amp;F&amp;&quot;Calibri,Normal&quot;&#10;HORA  DE  GENERACIÓN: &amp;&quot;Calibri,Negrita&quot;&amp; "
		options = options & time()
		options = options & "&amp;C&amp;&quot;Calibri,Negrita&quot;&amp;8ADMINISTRACIÓN GENERAL DE AUDITORÍA FISCAL FEDERAL&amp;&quot;Calibri,Normal&quot;&amp;9&#10;&amp;8MODELO DE EVASIÓN DEL CONTRIBUYENTE&amp;D&amp;6&amp;P DE &amp;#'/>"
    	options = options & "<PageMargins x:Bottom='0.75' x:Left='0.35' x:Right='0.35' x:Top='1.0'/>"
   		options = options & "</PageSetup>"
   		options = options & "<Print>"
	    options = options & "<ValidPrinterInfo/>"
    	options = options & "<HorizontalResolution>600</HorizontalResolution>"
    	options = options & "<VerticalResolution>600</VerticalResolution>"
   		options = options & "</Print>"
   		options = options & "<Selected/>"
   		options = options & "<Panes>"
	    options = options & "<Pane>"
     	options = options & "<Number>2</Number>"
     	options = options & "<ActiveRow>3</ActiveRow>"
     	options = options & "<ActiveCol>10</ActiveCol>"
    	options = options & "</Pane>"
   		options = options & "</Panes>"
   		options = options & "<ProtectObjects>False</ProtectObjects>"
   		options = options & "<ProtectScenarios>False</ProtectScenarios>"
  		options = options & "</WorksheetOptions>"
		Response.Write( options )
end function
	
	
public function setNota( nota )
		Response.Write("<Row ss:Height='24.75'>")
			Response.Write("<Cell ss:MergeAcross='4' ss:StyleID='s102'>")
				Response.Write("<Data ss:Type='String'>Notas</Data>")
			Response.Write("</Cell>")
		Response.Write("</Row>")
		Response.Write("<Row ss:Height='24.75'>")
			Response.Write("<Cell ss:MergeAcross='4' ss:MergeDown='1' ss:StyleID='m78529244'>")
				Response.Write("<Data ss:Type='String'>"&nota&"</Data>")
			Response.Write("</Cell>")
		Response.Write("</Row>")
end function
	
public function setNoteTitle( title )
		Response.Write("<Row ss:Height='24.75'>")
			Response.Write("<Cell ss:MergeAcross='7' ss:StyleID='notaTitulo'>")
				Response.Write("<Data ss:Type='String'>"&title&"</Data>")
			Response.Write("</Cell>")
		Response.Write("</Row>")
end function
	
public function setNote1( note )
		Response.Write("<Row ss:Height='11.25'>")
			Response.Write("<Cell ss:MergeAcross='7' ss:StyleID='nota1'>")
				Response.Write("<Data ss:Type='String'>"&note&"</Data>")
			Response.Write("</Cell>")
		Response.Write("</Row>")
end function
	
public function setNote2( note )
		Response.Write("<Row ss:AutoFitHeight='0'>")
			Response.Write("<Cell ss:MergeAcross='7' ss:MergeDown='1' ss:StyleID='nota2'>")
				Response.Write("<Data ss:Type='String'>"&note&"</Data>")
			Response.Write("</Cell>")
		Response.Write("</Row>")
end function
	
public function setNote3( note )
		Response.Write("<Row ss:AutoFitHeight='0' ss:Height='11.25' />")
		Response.Write("<Row ss:AutoFitHeight='0'>")
			Response.Write("<Cell ss:MergeAcross='7' ss:MergeDown='2' ss:StyleID='nota3'>")
				Response.Write("<Data ss:Type='String'>"&note&"</Data>")
			Response.Write("</Cell>")
		Response.Write("</Row>")
end function
	
public function setNoteTitle_A3( title )
		Response.Write("<Row ss:Height='24.75'>")
			Response.Write("<Cell ss:MergeAcross='8' ss:StyleID='notaTitulo'>")
				Response.Write("<Data ss:Type='String'>"&title&"</Data>")
			Response.Write("</Cell>")
		Response.Write("</Row>")
end function
	
public function setNote1_A3( note )
		Response.Write("<Row ss:Height='11.25'>")
			Response.Write("<Cell ss:MergeAcross='8' ss:StyleID='nota_A3'>")
				Response.Write("<Data ss:Type='String'>"&note&"</Data>")
			Response.Write("</Cell>")
		Response.Write("</Row>")
end function
	
public function setNote2_A3( note )
		Response.Write("<Row ss:Height='11.25'>")
			Response.Write("<Cell ss:MergeAcross='8' ss:StyleID='nota_A3'>")
				Response.Write("<Data ss:Type='String'>"&note&"</Data>")
			Response.Write("</Cell>")
		Response.Write("</Row>")
end function
	
public function setNote3_A3( note )
		Response.Write("<Row ss:Height='11.25'>")
			Response.Write("<Cell ss:MergeAcross='8' ss:StyleID='nota_A3'>")
				Response.Write("<Data ss:Type='String'>"&note&"</Data>")
			Response.Write("</Cell>")
		Response.Write("</Row>")
end function

'************************Indice del Libro *****************'
	
public function workBookIndex()

		sheet = sheet & "<Row>"
    	sheet = sheet & "<Cell ss:StyleID='s121' ss:HRef="&chr(34)&"#'General'!A1"&chr(34)&">"
		sheet = sheet & "<Data ss:Type='String'>General</Data>"
		sheet = sheet & "</Cell>"
		sheet = sheet & "<Cell ss:StyleID='s136'>"
		sheet = sheet & "<Data ss:Type='String'>DATOS GENERALES</Data>"
		sheet = sheet & "</Cell>"
    	sheet = sheet & "</Row>"
		sheet = sheet & "<Row ss:AutoFitHeight='0' ss:Height='7.5'>"
    	sheet = sheet & "<Cell ss:Index='3' ss:StyleID='s175' />"
   		sheet = sheet & "</Row>"

		sheet = sheet & "<Row>"
    	sheet = sheet & "<Cell ss:StyleID='s121' ss:HRef="&chr(34)&"#'Anexo I-A'!A1"&chr(34)&">"
			sheet = sheet & "<Data ss:Type='String'>Anexo I-A</Data>"
		sheet = sheet & "</Cell>"
		sheet = sheet & "<Cell ss:StyleID='s136'>"
		sheet = sheet & "<Data ss:Type='String'>ANEXO I A: INFORMACIÓN DE CRÉDITOS PENDIENTES</Data>"
		sheet = sheet & "</Cell>"
    	sheet = sheet & "</Row>"
		sheet = sheet & "<Row ss:AutoFitHeight='0' ss:Height='7.5'>"
    	sheet = sheet & "<Cell ss:Index='3' ss:StyleID='s175' />"
   		sheet = sheet & "</Row>"

	
		Response.Write( sheet )
end function
	
public function generalData( Datos )
		
		data = "<Row ss:StyleID='s141'>"
		data = data & "<Cell ss:Index='1' ss:StyleID='s401'>"
		data = data & "<Data ss:Type='String'>R.F.C.:</Data><NamedCelldata = data & ss:Name='Print_Area'/>"
	  	data = data & "</Cell>"
		
		data = data & "<Cell ss:MergeAcross='1' ss:StyleID='s698'>"
		data = data & "<Data ss:Type='String'>"&Datos(0,0)&"</Data><NamedCell ss:Name='Print_Area'/>"
	  	data = data & "</Cell>"
    	
		data = data & "<Cell ss:StyleID='s401'>"
		data = data & "<Data ss:Type='String'>CURP:</Data><NamedCell ss:Name='Print_Area'/>"
		data = data & "</Cell>"
    
		data = data & "<Cell ss:MergeAcross='1' ss:StyleID='s698'>"
		data = data & "<Data ss:Type='String'>"&Datos(1,0)&"</Data><NamedCell ss:Name='Print_Area'/>"
	  	data = data & "</Cell>"
		
		data = data & "<Cell ss:StyleID='s401'>"
		data = data & "<Data ss:Type='String'>ACTIVIDAD:</Data><NamedCell ss:Name='Print_Area'/>"
		data = data & "</Cell>"
    
		data = data & "<Cell ss:MergeAcross='3' ss:StyleID='s698'>"
		data = data & "<Data ss:Type='String'></Data><NamedCell ss:Name='Print_Area'/>"
		data = data & "</Cell>"
		
   		data = data & "</Row>"
		
	

		Response.Write( data )

end function
	
	
public function sectionTitle( sectionTitle_ )
	 sheet = sheet & "<Row >"
	 sheet = sheet & "<Cell ss:MergeAcross='9'>"
	 sheet = sheet & "<Data ss:Type='String'></Data></Cell>"
	 sheet = sheet & "</Row>"
	 sheet = sheet & "<Row >"
	 sheet = sheet & "<Cell ss:MergeAcross='9' ss:StyleID='s62'>"
	 sheet = sheet & "<Data ss:Type='String'>"&sectionTitle_&"</Data></Cell>"
	 sheet = sheet & "</Row>"
	 Response.Write( sheet )
end function

public function sectionCategory( sectionTitle_ )
	 sheet = sheet & "<Row >"
	 sheet = sheet & "<Cell ss:MergeAcross='9'>"
	 sheet = sheet & "<Data ss:Type='String'></Data></Cell>"
	 sheet = sheet & "</Row>"
	 sheet = sheet & "<Row >"
	 sheet = sheet & "<Cell ss:MergeAcross='9' ss:StyleID='s401'>"
	 sheet = sheet & "<Data ss:Type='String'>"&sectionTitle_&"</Data></Cell>"
	 sheet = sheet & "</Row>"
	 Response.Write( sheet )
end function

public function categoriaSolicitante()
	
	 sheet = sheet & "<Row ss:Height='16'>"
	 sheet = sheet & "<Cell ss:MergeAcross='4' ss:StyleID='s1001'>"
	 sheet = sheet & "<Data ss:Type='String'>Detalle de Proveedores </Data></Cell>"
	 sheet = sheet & "<Cell ss:MergeAcross='2' ss:StyleID='s1001'>"
	 sheet = sheet & "<Data ss:Type='String'>Información del Solicitante</Data></Cell>"
	 sheet = sheet & "<Cell ss:MergeAcross='7' ss:StyleID='s1001'>"
	 sheet = sheet & "<Data ss:Type='String'>Información de los Proveedores</Data></Cell>"
	 sheet = sheet & "<Cell ss:MergeAcross='33' ss:StyleID='s1001'>"
	 sheet = sheet & "<Data ss:Type='String'></Data></Cell>"
	 sheet = sheet & "</Row>"
	 Response.Write( sheet )
end function
	
public function mandatoryReport( mandatoryTitle , valor)
	sheet = "<Row ss:Height='25'>"
    sheet = sheet & "<Cell ss:MergeAcross='4' ss:StyleID='s66'>"
	sheet = sheet & "<Data ss:Type='String'>"&mandatoryTitle&"</Data></Cell>"
	if( valor <> "" ) then
    	sheet = sheet & "<Cell ss:MergeAcross='4' ss:StyleID='s64'><Data ss:Type='String'>"&formatCurrency(valor,0)&"</Data></Cell>"
	else
		sheet = sheet & "<Cell ss:MergeAcross='4' ss:StyleID='s64'><Data ss:Type='String'></Data></Cell>"
	end if
    sheet = sheet & "</Row>"
	Response.Write( sheet )
end function

	
	'**********************************************************************************************
	
	'**********************************************************************************************
	
end class

%>